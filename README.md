![Siyo](https://free.plopgrizzly.com/siyo/banner.svg)

# About
Cross-platform system interface for hardware IO.  Use screens, speakers, microphones, input devices, etc.

## Motivation
There needs to be crate that can handle all of the common tasks that are currently not portable across different platforms.

## Features
Each interface is represented by a module in Siyo.  Here's a list of all the modules:

* math - Fixed point, SIMD, GPU, Math
* clock - Date, Time of day, Timer
* hid - Human Interface Device, USB
* screen - Make a window
* cam - Camera, Webcam
* speaker - Play sound through speakers
* mic - Record sound through microphone
* net - Network
* drive - Persistent Storage

## Naming
Siyo is a cross-platform system interface for hardware IO.  If you try to say that really fast and
*accidently* miss a couple syllables/words it might sound like siyo (S+AH+YO).

## Getting Started
* `cargo install siyo` - install siyo
* `siyo update` - update siyo
* `siyo init` - initialize this directory as an empty siyo project.
* `siyo run` - build & run the program.

## Links
* [Website](https://free.plopgrizzly.com/siyo)
* [Cargo](https://crates.io/crates/siyo)
* [Documentation](https://docs.rs/siyo)
* [Change Log](https://free.plopgrizzly.com/siyo/changelog)
* [Contributing](https://plopgrizzly.com/contributing)
* [Code of Conduct](https://free.plopgrizzly.com/siyo/codeofconduct)

---

[![Plop Grizzly](https://plopgrizzly.com/images/logo-bar.png)](https://plopgrizzly.com)

# Siyo 0.1.0
* Support graphics on XCB (Linux) and JavaScript Canvas (Web Assembly).

# Siyo 0.0.0
* Initial release.
